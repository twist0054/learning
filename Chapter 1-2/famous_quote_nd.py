author = 'Oscar Wilde'
quote = '"Be yourself, everyone else is already taken."'
final_quote = f"{author} once said, {quote}"
print(final_quote)