
#INTEGERS
#add
print(3+2)
#substract
print(3-2)
#multiply
print(2*3)
#divide
print(3/2)
#exponents
print(3 ** 2)
print(3 ** 3)
print(10 ** 6)
#order in the operations
print(2 + 3*4)
print((2 + 3) * 4)

#FLOATS
print(0.1 + 0.1)
print(0.2 + 0.2)
print(2 * 0.1)
print(2 * 0.2)

print(0.2 + 0.1)
print(3 * 0.1)

#INTEGERS AND FLOATS
print(4/2)
print(1+2.0)
print(2 * 3.0)
print(3.0 ** 2)

universe_age = 14_000_000_000
print(universe_age)

x, y, z = 1, 2, 3
print(x)
print(y)
print(z)

MAX_CONECTIONS = 5000
print(MAX_CONECTIONS)

