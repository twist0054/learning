motorcycles = ['honda', 'yamaha', 'suzuki']
print(motorcycles)
print(motorcycles[2])
print(motorcycles[-1])

motorcycles[0] = 'ducati'
print(motorcycles)

motorcycles.append('honda')
print(motorcycles)


motorcycles2 = []

motorcycles2.append('honda')
motorcycles2.append('yamaha')
motorcycles2.append('suzuki')
motorcycles2.append('ducati')

print(motorcycles2)

del motorcycles2[1]
print(motorcycles2)

popped_motorcycles = motorcycles2.pop()
print(motorcycles2)
print(popped_motorcycles)

last_owned = motorcycles2.pop()
print(f"The last motorcycle I owned was a {last_owned.title()}")

first_owned = motorcycles2.pop(0)
print(f"The first motocycle I owned was a {first_owned.title()}")
print(motorcycles)
motorcycles.remove('ducati')
print(motorcycles)

to_expensive = 'honda'
motorcycles.remove(to_expensive)
print(motorcycles)
print(f"\nA {to_expensive.title()} is too expensive for me.")

